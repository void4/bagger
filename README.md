# TopRace Excavator control

The excavator remote control has 15 buttons, it uses a 4x4 button matrix because its microcontroller only have 16 pins, of which at least 3 are used for:
- the status LED
- the antenna
- power supply

There seem to be two main options to control the remote with the Arduino:
- solder a transistor to each button and control the base pin via a wire to an Arduino pin
- connect the matrix row and column pins of the microcontroller directly to the Arduino

I chose the latter - it requires simulating a keyboard matrix.

The complication with this is that the microcontroller only scans one row at a time, so you have to be reasonably fast. This turned out to be possible. This was also the reason I chose not to use PyFirmata to control the Arduino directly, instead writing Arduino code and flashing it, building in a serial protocol for communication.

The conductor tracks labeled U1 are the outputs, pins 9 to 12 (inclusive) have to be written to in order to simulate the keyboard matrix.

Another feature/complication of the remote control is that it stops scanning the rows after 5 seconds of inactivity (no buttons pressed). To reactivate scanning, a button has to be pressed.

The row scanning interval seemed to be around 5ms. I chose not to use a timing approach, but just change the state of the column pins when the row had already changed. This seems to be no problem if it is done fast enough.

program.ino contains the arduino code

If you plan on running your toy on letsrobot.tv:
bagger.py and command.py should be put inside letsrobot/hardware/

command.py handles the serial communication with the Arduino, bagger.py just imports it and sends it the chat commands.

In your letsrobot.conf, set your owner/user name, the robot id and the camera id and set the motor controller type to bagger:
`type=bagger`

You can also manually test commands in the Arduino IDE Serial Monitor:
The serial protocol is:
`<number of buttons>,<button1 row>,<button1 column>,<button2 row>, <button2 column>,...`

For example: `1,3,1` for closing the claw or `1,0,0` to turn on/off the sound