import serial

for path in ["/dev/ttyACM0", "/dev/ttyACM1"]:
	try:
		ser = serial.Serial(path)
		ser.baudrate = 1000000
		break
	except:
		pass

SND = [0,0]
A1 = [3,3]
A2 = [3,2]
B1 = [1,3]
B2 = [0,1]
GO = [2,0]
GC = [3,1]

cmdmap = {
	"": [],
	"A": A1+B2,
	"W": A1+B1,
	"S": A2+B2,
	"D": A2+B1,
	"VL": [1,2],
	"VR": [],
	"AU": [1,0],
	"AD": [2,1],
	"BU": [0,2],
	"BD": [0,3],
	"GO": GO,
	"GC": GC,
}

lastcmd = None

def run(cmd):
	global lastcmd



	if not cmd in cmdmap:
		return

	commands = cmdmap[cmd]

	if commands != lastcmd:
		command = str(len(commands)//2) + "," + ",".join([str(x) for x in commands]) + "\n"
		print("RUN", cmd, command, end="")
		ser.write(command.encode("utf8"))
		print("READ", ser.readline())
		lastcmd = commands

	#while True:
	#	print(ser.readline())

from time import sleep

if __name__ == "__main__":
	sleep(2)
	run("W")
	sleep(3)
	run("")
