#include <LinkedList.h>

LinkedList<int> activeButtons = LinkedList<int>();

int rows[4] = {6,7,8,9};
int columns[4] = {2,3,4,5};

void setup() {
  for (int r=0; r<4; r++) {
    pinMode(rows[r], INPUT_PULLUP);
  }
  for (int c=0; c<4; c++) {
    pinMode(columns[c], OUTPUT);

    //Insight: https://forum.arduino.cc/index.php?topic=555202.0
    // Have to set them to HIGH, put them on low when correct row is strobed
    digitalWrite(columns[c], HIGH);
  }
  
  Serial.begin(1000000);

  //activeButtons.add(3);
  //activeButtons.add(2);
  //delay(5000);
}

/* Splits string at separator, returns index' element in the list */
String getValue(String data, char separator, int index)
{
    int maxIndex = data.length() - 1;
    int j = 0;
    String chunkVal = "";

    for (int i = 0; i <= maxIndex && j <= index; i++)
    {
        chunkVal.concat(data[i]);

        if (data[i] == separator)
        {
            j++;

            if (j > index)
            {
                chunkVal.trim();
                return chunkVal;
            }

            chunkVal = "";
        }
        else if ((i == maxIndex) && (j < index)) {
            chunkVal = "";
            return chunkVal;
        }
    }   
}


String inData;

// used for estimating strobe length
unsigned long start = 0;

// stores last column activation value
int lasttotal = 0;

/*
 * 1110 - 14
 * 1101 - 13
 * 1011 - 11
 * 0111 - 7
 */
int rowvalues[4] = {14, 13, 11, 7};

// used for blinking it into live again
int activated = 0;

void loop() {

  while (Serial.available() > 0) {
    char received = Serial.read();
    inData += received;

    if (received == '\n') {
      activeButtons.clear();

      // watch for heap overflow?
      int numpairs = getValue(inData, ',', 0).toInt();
      for (int i=0; i<numpairs*2; i++) {
        activeButtons.add(getValue(inData, ',', i+1).toInt());
      }

      // Return list over serial for verification
      // list values in order, then list length
      for (int i=0; i<activeButtons.size(); i++) {
        Serial.print(activeButtons.get(i));
        Serial.print(",");
      }
      Serial.println(activeButtons.size());
      inData = "";
    }
  }

  // Read all columns  
  int total = 0;
  for (int r=0;r<4;r++) {
    int v = digitalRead(rows[r]);
    total |= v << r;
  }

  //Serial.println(total);

  /* Print read columns bitwise
  for (int b = 7; b >= 0; b--) {
    Serial.print(bitRead(total, b));
  }
  
  Serial.println("");
  */

  /*Measure strobe interval
  if (total == rowvalues[0]) {
    if (lasttotal != total) {
      start = micros();
    }
  } else {
    if (lasttotal == rowvalues[0]) {
      Serial.println(micros()-start);
    }
  }

  lasttotal = total;
  */

  //Serial.println(total);

  // The sneaky/ultraintelligent chip stops scanning after five seconds of no input.
  // so we have to reactivate it by pressing something
  if (total == rowvalues[0] || total == rowvalues[1] || total == rowvalues[2] || total == rowvalues[3]) {//&& activated == 1

    //set all to HIGH
    for (int c=0; c<4; c++) {
       
       // Only set column to HIGH if it is not potential active row
       if (activeButtons.size() >= 2 && activeButtons.get(1) == c) {
        continue;
       }
       
       if (activeButtons.size() >= 4 && activeButtons.get(3) == c) {
        continue;
       }
       
       digitalWrite(columns[c], HIGH);
       
    }

  } else {
    
    activated += 1;

    // Blink the hell out of it until it notices
    if (activated % 100 == 0) {
      digitalWrite(columns[0], LOW);
    } else {
      digitalWrite(columns[0], HIGH);
    }
  }

  //Serial.println(activeButtons.size());
  for (int i=0; i<activeButtons.size(); i+=2) {
    
    int rowtarget = activeButtons.get(i);
    int columntarget = activeButtons.get(i+1);
    
    if (total == rowvalues[rowtarget]) {
      digitalWrite(columns[columntarget], LOW);
    } else {
      digitalWrite(columns[columntarget], HIGH);
    }
  }


}
